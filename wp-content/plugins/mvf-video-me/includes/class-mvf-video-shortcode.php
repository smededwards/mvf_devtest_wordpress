<?php

add_shortcode('prefix-video', [Mvf_Video_Post_Type_Shortcode::class, 'video_shortcode']);

class Mvf_Video_Post_Type_Shortcode extends Mvf_Video_Post_Type {

  public static function video_shortcode($atts) {
    extract(shortcode_atts(array(
      'id' => null,
      'type' => 'videos',
      'posts' => -1,
      'border_color'=>'#3498db',
      'border_width'=>'8px'
    ), $atts));

    $args = array(
      'post_type' => $type,
      'post_status' => 'publish',
      'post__in' => array($id),
      'posts_per_page' => $posts,
      'paged' => get_query_var('paged')
    );
    $the_query = new WP_Query($args); ?>

    <div class="mvf-video-cpt">
      <?php if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <?php $subtitle = get_post_meta( get_the_ID(), 'video_subtitle', TRUE );?>
        <?php $description = get_post_meta( get_the_ID(), 'video_description', TRUE );?>
        <div class="mvf-video-cpt" style="border-width: <?php echo $border_width; ?>; border-color: <?php echo $border_color ?>; border-style: solid;">
          <div class="mvf-video-cpt__thumbnail">
          <?php if(has_post_thumbnail()){ ?>
            <img class="mvf-video-cpt__thumbnail--img" src="<?php the_post_thumbnail_url('medium'); ?>" alt="" />
          <?php } else { ?>
            <img class="mvf-video-cpt__thumbnail--img" src="<?php echo MVF_VIDEO_ME_PLUGIN_URL ?>/assets/img/video--placeholder.png" alt="" />
          <?php } ?>
          </div>
          <div class="mvf-video-cpt__content">
            <h2 class="mvf-video-cpt__title">
              <?php the_title(); ?>
            </h2>
            <h3 class="mvf-video-cpt__subtitle">
              <?php echo $subtitle ?>
            </h3>
            <p class="mvf-video-cpt__description">
              <?php echo $description ?>
            </p>
          </div>
        </div>
      <?php endwhile; wp_reset_postdata(); ?>
    </div>
    
  <?php endif; }
}
new Mvf_Video_Post_Type_Shortcode();
