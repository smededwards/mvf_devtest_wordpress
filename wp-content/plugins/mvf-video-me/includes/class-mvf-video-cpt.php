<?php

class Mvf_Video_Post_Type {
	public $name = 'Videos';
	public $singular = 'Video';

	public function __construct() {
		add_action('init', array($this, 'create_custom_post_type'));
	}

	public function create_custom_post_type() {
		$this->labels = array(
			'name'               => __($this->name, 'mvf-cpt-videos'),
			'singular_name'      => __($this->singular, 'mvf-cpt-videos'),
			'menu_name'          => __($this->name, 'mvf-cpt-videos'),
			'name_admin_bar'     => __($this->singular, 'mvf-cpt-videos'),
			'add_new'            => __('Add New ' . $this->singular),
			'add_new_item'       => __('Add New ' . $this->singular),
			'new_item'           => __('New ' . $this->singular),
			'edit_item'          => __('Edit ' . $this->singular),
			'view_item'          => __('View ' . $this->singular),
			'view_items'         => __('View ' . $this->name),
			'all_items'          => __('All ' . $this->name),
			'search_items'       => __('Search ' . $this->name),
			'parent_item_colon'  => __('Parent ' . $this->name),
			'not_found'          => __('No ' . $this->name . 'found.'),
			'not_found_in_trash' => __('No ' . $this->name . 'found in Trash.')
		);

		$this->args = array(
			'labels'             => $this->labels,
			'description'        => __('Custom post type for' . $this->name),
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'has_archive'        => true,
			'hierarchical'       => true,
			'supports'           => array('title', 'thumbnail', 'revisions'),
			'menu_icon'          => 'dashicons-video-alt3',
			'menu_position'      => 10,
		);
		register_post_type($this->name, $this->args);
	}
}
new Mvf_Video_Post_Type();
