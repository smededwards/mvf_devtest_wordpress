<?php
class Mvf_Video_Post_Type_Metaboxes extends Mvf_Video_Post_Type {
	
	public function init() {
		add_action('add_meta_boxes', array($this, 'video_meta_boxes'));
		add_action('save_post', array($this, 'save_meta_boxes'),  10, 2);
	}

	public function video_meta_boxes() {
		add_meta_box(
			'video_fields',
			'Video Fields',
			array($this, 'render_meta_boxes'),
			'videos',
			'normal',
			'high'
		);
	}

	public function render_meta_boxes($post) {
		$meta = get_post_custom($post->ID);
		$subtitle = !isset($meta['video_subtitle'][0]) ? '' : $meta['video_subtitle'][0];
		$description = !isset($meta['video_description'][0]) ? '' : $meta['video_description'][0];
		$type = !isset($meta['video_type'][0]) ? '' : $meta['video_type'][0];

		wp_nonce_field(basename(__FILE__), 'video_fields'); ?>

		<table class="form-table">
			<tr>
				<td class="team_meta_box_td" colspan="2">
					<label for="video_subtitle">
						<?php _e('Subtitle', 'mvf-video-me'); ?>
					</label>
				</td>
				<td colspan="4">
					<input type="text" id="video_subtitle" name="video_subtitle" class="regular-text" value="<?php echo $subtitle; ?>">
				</td>
			</tr>
			<tr>
				<td class="team_meta_box_td" colspan="2">
					<label for="video_description"><?php _e('Description', 'mvf-cpt-videos'); ?>
					</label>
				</td>
				<td colspan="4">
					<textarea rows="5" id="video_description" name="video_description" class="regular-text"><?php echo $description; ?></textarea>
				</td>
			</tr>
			<tr>
				<td class="team_meta_box_td" colspan="2">
					<label for="video_id"><?php _e('Video ID', 'mvf-cpt-videos'); ?>
					</label>
				</td>
				<td colspan="4">
					<input id="video_id" type="text" name="video_id" readonly="readonly" class="regular-text" value="<?php echo $post->ID ?>">
				</td>
			</tr>
			<tr>
				<td class="team_meta_box_td" colspan="2">
					<label for="video_id"><?php _e('Shortcode', 'mvf-cpt-videos'); ?>
					</label>
				</td>
				<td colspan="4">
					<input id="video_id" type="text" name="video_id" readonly="readonly" class="regular-text" value="prefix-video id='<?php echo $post->ID ?>'">
				</td>
			</tr>
			<tr>
				<td class="team_meta_box_td" colspan="2">
					<label for="video_type"><?php _e('Type', 'mvf-cpt-videos'); ?>
					</label>
				</td>
				<td colspan="4">
					<select id="video_type" name="video_type" class="regular-text">
						<option value="YouTube" <?php selected( $type, 'YouTube' ); ?>>
							YouTube
						</option>
						<option value="Vimeo" <?php selected( $type, 'Vimeo' ); ?>>
							Vimeo
						</option>
						<option value="Dailymotion" <?php selected( $type, 'Dailymotion' ); ?>>
							Dailymotion
						</option>
					</select>
				</td>
			</tr>
		</table>
	<?php }

	public function save_meta_boxes($post_id) {
		global $post;

		// Verify nonce
		if (!isset($_POST['video_fields']) || !wp_verify_nonce($_POST['video_fields'], basename(__FILE__))) {
			return $post_id;
		}

		// Check Autosave
		if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit'])) {
			return $post_id;
		}

		// Don't save if only a revision
		if (isset($post->post_type) && $post->post_type == 'revision') {
			return $post_id;
		}

		// Check permissions
		if (!current_user_can('edit_post', $post->ID)) {
			return $post_id;
		}

		// Update Meta Fields
		if( isset( $_POST[ 'video_subtitle' ] ) ) {
			update_post_meta( $post_id, 'video_subtitle', sanitize_text_field( $_POST[ 'video_subtitle' ] ) );
		} else {
			delete_post_meta( $post_id, 'video_subtitle' );
		}
		if( isset( $_POST[ 'video_description' ] ) ) {
			update_post_meta( $post_id, 'video_description', sanitize_text_field( $_POST[ 'video_description' ] ) );
		} else {
			delete_post_meta( $post_id, 'video_description' );
		}
		if( isset( $_POST[ 'video_type' ] ) ) {
			update_post_meta( $post_id, 'video_type', sanitize_text_field( $_POST[ 'video_type' ] ) );
		} else {
			delete_post_meta( $post_id, 'video_type' );
		}
	}
}
