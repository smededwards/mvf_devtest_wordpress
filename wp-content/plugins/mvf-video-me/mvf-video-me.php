<?php

/**
 * Plugin name: MVF Custom Post Type - Videos
 * Plugin URI: https://bitbucket.org/smededwards/mvf_devtest_wordpress/
 * Author: Michael Edwards
 * Author URI: https://smedededwards.com
 * Description: WordPress Developer Test for MVF
 * Version: 0.1.0
 * Text Domain: mvf-video-me
 * Domain Path: /languages
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
	exit;
}

// Plugin Directory Constant
define('MVF_VIDEO_ME_PLUGIN_DIR', plugin_dir_path(__FILE__));

// Plugin URL Constant
define('MVF_VIDEO_ME_PLUGIN_URL', plugin_dir_url(__FILE__));

// Register Custom Post Type
require_once MVF_VIDEO_ME_PLUGIN_DIR . 'includes/class-mvf-video-cpt.php';

// Add Meta Boxes
require_once MVF_VIDEO_ME_PLUGIN_DIR . 'includes/class-mvf-video-metaboxes.php';

// Add Shortcode
require_once MVF_VIDEO_ME_PLUGIN_DIR . 'includes/class-mvf-video-shortcode.php';

// Enqueue CSS
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style( 'mvf-video-fe-styles', plugins_url('/assets/css/mvf-video-me-styles.css', __FILE__) );
});

// Initialize metaboxes
$post_type_metaboxes = new Mvf_Video_Post_Type_Metaboxes;
$post_type_metaboxes->init();
