# WordPress Developer Test

by Michael Edwards

## Tasks

- [x] Create a custom plugin
- [x] A plugin should create a custom post type called: Videos
- [x] CPT must have these fields for admin: Title, Subtitle, Description, Video ID, and Type (options are Youtube, Vimeo and Dailymotion - dropdown or radio buttons)
- [x] CPT should not have its single page for readers and should not be in menu navs for Authors
- [x] Create a shortcode that will display a video CPT
- [x] Default border should be 8 pixels wide
- [x] Video output should be responsive
- [x] Layout should be responsive
- [ ] Create shortcode generator - TinyMCE button that generates a popup with following fields: post id, border width and border color with color picker option
- [x] Plugin must be fully translatable
- [x]  Code must strictly follow the WP coding standards
- [x]  Code should not raise any warnings or notices

## Comments
I have managed to complete the required tasks, except the TinyMCE Shortcode Generator. I don't think this is a task I've done for many years, and with the Classic Editor seemingly being deprecated, it doesn't seem like something that would come up too often. However, this would be quite straightforward to do within Gutenberg.
